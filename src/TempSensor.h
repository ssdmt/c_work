/*
 * TempSensor.h
 *
 *  Created on: Apr 16, 2017
 *      Author: ssdmt
 */

#ifndef TEMPSENSOR_H_
#define TEMPSENSOR_H_

namespace std {

class CTempSensor {
public:
	CTempSensor();
	virtual ~CTempSensor();
};

} /* namespace std */

#endif /* TEMPSENSOR_H_ */
